#!/usr/bin/env bash
# vi :set ft=bash:

set -euo pipefail


install_project () {
  local project_repo project_dir
  project_repo="$1"
  project_dir="$2"

  if [ -z "${project_repo}" ] || [ -z "${project_dir}" ]; then
    cat <<USAGE
install_project project_repo project_dir

project_repo
  URL to Git repo containing project
project_dir
  Absolute path to project workarea
USAGE
    exit 1
  fi

  if [ -d "${project_dir}" ]; then
    pushd "${project_dir}" >> /dev/null 2>&1
    git pull
    popd >> /dev/null 2>&1
  else
    git clone "${project_repo}" "${project_dir}"
  fi
}


setup_project () {
  local project_dir
  project_dir="$1"

  pushd "${project_dir}" >> /dev/null 2>&1
  [ -f "./ci/scripts/init_pipeline.sh" ] && ./ci/scripts/init_pipeline.sh
  popd >> /dev/null 2>&1
}


if [ "$0" == "$BASH_SOURCE" ]; then
  PROJECTREPO="${PROJECTREPO:-https://gitlab.com/streams1/scratch.git}"
  PROJECTDIR="${PROJECTDIR:-scratch}"
  install_project "${PROJECTREPO}" "${PROJECTDIR}"
  setup_project "${PROJECTDIR}"
fi
