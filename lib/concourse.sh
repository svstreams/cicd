#!/usr/bin/env bash
# vi :set ft=bash:

set -euo pipefail

CONCOURSE_PORT="${CONCOURSE_EXT_PORT:-9080}"

errcho () {
  1>&2 echo "$@";
}

wait_on_server () {
  local url sleep_for cycles
  url=$1
  sleep_for=${2:-5}
  cycles=${3:-12}

  until [[ $cycles -eq 0 ]] || curl --output /dev/null --silent --head --fail "${url}"; do
    sleep $sleep_for
    cycles=$((cycles-1))
  done

  [[ cycles -gt 0 ]]
}



install_concourse_ci () {
  local cci_inst="/opt/concourse_ci"

  mkdir -p "${cci_inst}"
  pushd "${cci_inst}"

  rm -rf docker-compose.yml
  wget https://concourse-ci.org/docker-compose.yml
  sed -i -e "s/8080/${CONCOURSE_PORT}/" docker-compose.yml    # Patch alternate port to suit my host
  sed -i -e '/restart:/ {:l;/^\s*$/!{n;bl}}' -e 's/.*environment:.*/    restart: unless-stopped\n&/' docker-compose.yml

  docker-compose up --quiet-pull -d
  popd

}

install_concourse_fly_cli () {
  if docker ps | grep concourse_ci ; then
    if wait_on_server "http://localhost:${CONCOURSE_PORT}" 5 60; then
      curl -L "http://localhost:${CONCOURSE_PORT}/api/v1/cli?arch=amd64&platform=linux" >/usr/local/bin/fly
      chmod +x /usr/local/bin/fly
    else
      errcho "Concourse CI server not available"
    fi
  fi
  return 0
}

if [ "$0" == "$BASH_SOURCE" ]; then
  install_concourse_ci
  install_concourse_fly_cli
fi

