#!/usr/bin/env bash
# vi :set ft=bash:

install_user_tools() {
  sudo apt-get install -y git vim tree
}

if [ "$0" == "$BASH_SOURCE" ]; then
  install_user_tools
fi
