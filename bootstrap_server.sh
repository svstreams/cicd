#!/usr/bin/env bash
# vi :set ft=bash:

set -euo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
readonly DIR


. "${DIR}/lib/concourse.sh"
. "${DIR}/lib/docker.sh"
. "${DIR}/lib/stdpackages.sh"

if [ "$0" == "$BASH_SOURCE" ]; then
  install_user_tools
  install_docker
  install_docker-compose
  install_concourse_ci
  install_concourse_fly_cli
fi
